﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Kbots;

namespace TotalAnigilationExamples.Behaviours
{
    static class ShowKbotInfo
    {
        public static void Show() 
        {
            AK akKbot = new AK();
            Storm stormKbot = new Storm();
            Thud thudKbot = new Thud();
            Crasher crasherKbot = new Crasher();

            string kbotNumber = string.Empty;
            while (kbotNumber != "100")
            {
                Console.WriteLine("Выберите номер робота, по которому хотите получить информацию");
                Console.WriteLine("1 - AK");
                Console.WriteLine("2 - Storm");
                Console.WriteLine("3 - Thud");
                Console.WriteLine("4 - Crasher");
                Console.WriteLine("-------------");
                Console.WriteLine("100 - Выйти из программы");

                kbotNumber = Console.ReadLine();
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("");

                switch (kbotNumber)
                {
                    case "1":
                        Console.WriteLine("Информация для робота AK");
                        akKbot.ShowDetails();
                        break;
                    case "2":
                        Console.WriteLine("Информация для робота Storm");
                        stormKbot.ShowDetails();
                        break;
                    case "3":
                        Console.WriteLine("Информация для робота Thud");
                        thudKbot.ShowDetails();
                        break;
                    case "4":
                        Console.WriteLine("Информация для робота Crusher");
                        crasherKbot.ShowDetails();
                        break;
                }

                Console.WriteLine("----------------------------");

            }
        }
    }
}
