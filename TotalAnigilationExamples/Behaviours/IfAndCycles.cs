﻿using System;
using TotalAnigilationExamples.Behaviours;
using TotalAnigilationExamples.Kbots;

namespace TotalAnigilationExamples.Behaviours
{
    static class IfAndCycles
    {
        public static void ExampleWithTank()
        {
            bool haveCorectAnswer = false;

            while (haveCorectAnswer == false)
            {
                Console.WriteLine("у вас есть танк?");
                string response = Console.ReadLine();

                if (response == "да")
                {
                    Console.WriteLine("поехали мочить армов");
                    haveCorectAnswer = true;
                }
                else if (response == "нет")
                {
                    bool tankBuilt = false;
                    while (tankBuilt == false)
                    {
                        Console.WriteLine("строим танк");
                        Console.WriteLine("танк построен?");

                        bool internalCorrect = false;
                        string internalResponse = string.Empty;
                        while (internalCorrect == false)
                        {

                            internalResponse = Console.ReadLine();
                            if (internalResponse != "да" && internalResponse != "нет")
                            {
                                Console.WriteLine("введён некорректный ответ. повторите ввод");
                            }
                            else
                            {
                                internalCorrect = true;
                            }

                        }

                        if (internalResponse == "да")
                        {
                            Console.WriteLine("поехали мочить армов");
                            tankBuilt = true;
                        }
                        

                    }
                        
                    

                    haveCorectAnswer = true;
                }
                else
                {
                    Console.WriteLine("введён некоректный ответ:" + response + ", повторите");
                }

            }
        }    
    }
}
