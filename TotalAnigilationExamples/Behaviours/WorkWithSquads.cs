﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Vehicles;
using TotalAnigilationExamples.Kbots;

namespace TotalAnigilationExamples.Behaviours
{
    static class WorkWithSquads
    {
        static public void Act()
        { 
            Squad ivanSquad = new Squad();
            ivanSquad.Units.Add(new Weasel());
            Spoiler ivan = new Spoiler();
            ivanSquad.Units.Add(ivan);
            Thud gmgkop = new Thud();
            ivanSquad.Units.Add(gmgkop);

            //KbotLab kbotLab = new KbotLab();
            //var thud = kbotLab.CreateKbot(eKbotTypes.Thud);
            //var storm = kbotLab.CreateKbot(eKbotTypes.Storm);

            //var kBot = kbotLab.CreateKbot(eKbotTypes.Crasher);
        }
    }
}
