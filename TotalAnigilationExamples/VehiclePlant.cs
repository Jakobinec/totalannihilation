﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Vehicles;
using TotalAnigilationExamples;

namespace TotalAnigilationExamples
{
    class VehiclePlant
    {
        public Vehicle CreateVehicle(eVehicleTypes VehicleType)
        {
            switch (VehicleType)
            {
                case eVehicleTypes.Instigator:
                    return new Instigator();
                case eVehicleTypes.Slasher:
                    return new Slasher();
                case eVehicleTypes.Spoiler:
                    return new Spoiler();
                case eVehicleTypes.Weasel:
                    return new Weasel();
            }

            return null;
        }
    }
}
