﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TotalAnigilationExamples.Interfaces
{
    interface IGroundUnit
    {
        void Walk();
        void Fire();
        string GetName();

    }
}
