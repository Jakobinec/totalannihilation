﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Aircraft
{
        class Typhoon : Aircraft
        {
            public Typhoon()
            {
                Fraction = eFractions.CORE;
                Tag = "CORVSEAP";
                TaClass = eClass.AIRCRAFT;
                BuiltBy = "AircraftLab";
                TechLevel = 1;
                MetalCost = 545;
                EnergyCost = 9785;
                BuildTime = 23698;
                MaxVelocity = 126.0;
                Acceleration = 3.12;
                TurnRate = 84;
                Description = "The Typhoon is a level 1 torpedo seaplane that drops torpedoes in the same manner as the Titan; it is also equipped with air-to-air missiles, making it the only <bomber> that can attack other flying aircraft. It can also use the missiles to deal minor damage to land units, though its weak armor and long build time makes it impractical to be used for any purpose other than torpedo bombing. Its ARM counterpart is the Albatross.";
                LifePercent = 100;
            }
        }
}
