﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Aircraft
{
        class Avenger : Aircraft
        {
            public Avenger()
            {
                Fraction = eFractions.CORE;
                Tag = "CORVENG";
                TaClass = eClass.AIRCRAFT;
                BuiltBy = "AircraftLab";
                TechLevel = 1;
                MetalCost = 101;
                EnergyCost = 3181;
                BuildTime = 9196;
                MaxVelocity = 132.0;
                Acceleration = 2.52;
                TurnRate = 67;
                Description = "The Avenger is a level 1 fighter aircraft; it is armed with air-to-air missiles that are effective against other aircraft but deal insignificant damage to ground units. It is also better at taking hits than the Fink, but not by much. Its ARM counterpart is the Freedom Fighter.";
                LifePercent = 100;
            }
        }

}
