﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;


namespace TotalAnigilationExamples.Aircraft
{
    class Fink : Aircraft
    {
        public Fink()
        {
            Fraction = eFractions.CORE;
            Tag = "CORFINK";
            TaClass = eClass.AIRCRAFT;
            BuiltBy = "AircraftLab";
            TechLevel = 1;
            MetalCost = 36;
            EnergyCost = 1369;
            BuildTime = 2156;
            MaxVelocity = 132.0;
            Acceleration = 4.80;
            TurnRate = 115;
            Description = "The Fink is a level 1 scout aircraft; because of its high speed and good line of sight, it is often used for early scouting and maintaining visual coverage. It has no weapon and easily gets destroyed by one or two anti-air missiles though, so replacements are usually needed. It also has radar, however the radius is quite small. Its ARM counterpart is the Peeper.";
            LifePercent = 100;
        }
    }
}
