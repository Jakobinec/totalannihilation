﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Aircraft
{
        class Voodoo : Aircraft
        {
            public Voodoo()
            {
                Fraction = eFractions.CORE;
                Tag = "CORSFIG";
                TaClass = eClass.AIRCRAFT;
                BuiltBy = "AircraftLab";
                TechLevel = 1;
                MetalCost = 182;
                EnergyCost = 6524;
                BuildTime = 14857;
                MaxVelocity = 133.2;
                Acceleration = 4.56;
                TurnRate = 84;
                Description = "The Voodoo is a level 1 fighter seaplane; it is hardly different from the Avenger other than a slight increase in maneuverability and the ability to submerge underwater, which may be useful for escaping from or ambushing enemies. Its ARM counterpart is the Tornado.";
                LifePercent = 100;
            }
        }

}
