﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Aircraft
{
    class Shadow : Aircraft
    {
            public Shadow()
            {
                Fraction = eFractions.CORE;
                Tag = "CORSHAD";
                TaClass = eClass.AIRCRAFT;
                BuiltBy = "AircraftLab";
                TechLevel = 1;
                MetalCost = 131;
                EnergyCost = 5691;
                BuildTime = 10750;
                MaxVelocity = 96.0;
                Acceleration = 0.84;
                TurnRate = 55;
                Description = "The Shadow is a level 1 bomber aircraft; when engaging a target it will fly across it many times, each time dropping five bombs in its flight path which damage the target and any unit nearby, until the target is destroyed. The bombs pack quite a punch, however it is helpless against fighters and go down rather quickly in the face of ground-based anti-air defence. It is most effective in numbers. Its ARM counterpart is the Thunder.";
                LifePercent = 100;
            }
        }
}
