﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Aircraft
{
    class ConstructionSeaplane : Aircraft 
    {
            public ConstructionSeaplane()
            {
                Fraction = eFractions.CORE;
                Tag = "CORCSA";
                TaClass = eClass.AIRCRAFT;
                BuiltBy = "AircraftLab";
                TechLevel = 1;
                MetalCost = 125;
                EnergyCost = 5824;
                BuildTime = 11520;
                MaxVelocity = 75.6;
                Acceleration = 0.48;
                TurnRate = 19;
                Description = "The CORE Construction Seaplane is a level 1 unit that is capable of building both land and sea level 1 and 2 towers and structures. Being a seaplane, it has the same advantages as the Construction Aircraft plus one more: the ability to land into and hide under water; this is very useful when using the Construction Seaplane to repair ships at the front as it can quickly submerge to safety once done. It still has the weakest nanolathe just like a Construction Aircraft, and is more expensive. Each Construction Seaplane increases the player's energy and metal storage capacity by 50. Its counterpart is the ARM Construction Seaplane.";
                LifePercent = 100;
            }
        
    }
}
