﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Aircraft
{
        class Valkyrie : Aircraft
        {
            public Valkyrie()
            {
                Fraction = eFractions.CORE;
                Tag = "CORVALK";
                TaClass = eClass.AIRCRAFT;
                BuiltBy = "AircraftLab";
                TechLevel = 1;
                MetalCost = 115;
                EnergyCost = 2695;
                BuildTime = 5889;
                MaxVelocity = 84.0;
                Acceleration = 0.24;
                TurnRate = 24;
                Description = "The Valkyrie is a level 1 transport aircraft; its armour is relatively weak, so it is unsuited to transport units directly into the field of battle. The Valkyrie can't pick up ships or units that are submerged underwater, but can pick up hovercraft; it can also unload units into a body of water, provided that they can either submerge into or float above water masses. When compared to its land and sea counterparts, the Valkyrie is the least practical, as it can only carry one unit at a time, however, it is by far the fastest and is able to reach places that the other transportation units cannot. The ARM counterpart to the Valkyrie is the Atlas.";
                LifePercent = 100;
            }
        }
}
