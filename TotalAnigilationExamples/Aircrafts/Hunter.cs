﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Aircraft
{
    class Hunter : Aircraft 
    {
        public Hunter()
        {
            Fraction = eFractions.CORE;
            Tag = "CORHUNT";
            TaClass = eClass.AIRCRAFT;
            BuiltBy = "AircraftLab";
            TechLevel = 1;
            MetalCost = 142;
            EnergyCost = 7421;
            BuildTime = 10512;
            MaxVelocity = 116.4;
            Acceleration = 0.60;
            TurnRate = 67;
            Description = "The Avenger is a level 1 fighter aircraft; it is armed with air-to-air missiles that are effective against other aircraft but deal insignificant damage to ground units. It is also better at taking hits than the Fink, but not by much. Its ARM counterpart is the Freedom Fighter.";
            LifePercent = 100;
        }
    }
}
