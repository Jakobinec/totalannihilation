﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;
using TotalAnigilationExamples.Vehicles;

namespace TotalAnigilationExamples.Vehicles
{
    class Instigator : Vehicle
    {
        public Instigator()
        {
            Fraction = eFractions.CORE;
            Name = "Instigator";
            Tag = "CORATOR";
            TaClass = eClass.VEHICLE;
            BuiltBy = "VehiclePlant";
            TechLevel = 1;
            MetalCost = 110;
            EnergyCost = 887;
            BuildTime = 1737;
            MaxVelocity = 22.8;
            Acceleration = 0.24;
            TurnRate = 78;
            Description = "The Instigator is a level 1 tank that provides the basics of CORE tank warfare; fast and relatively small, the Instigator is useful in the early game for sneaking past other units and attacking the enemy base. The Instigator is armed with a light laser, which doesn't deal a lot of damage, but is very accurate, even when fired on the move. " +
                "This makes the Instigator quite useful against fast, lightly-armored units, such as the Peewee.Its ARM counterpart is the Flash.Although the Flash is considered superior to the Gator, their speed is similar, as well as their damage output and build times.An ARM player should never underestimate an Instigator's effectiveness as a raiding force";
            LifePercent = 100;
        }
    }
}
