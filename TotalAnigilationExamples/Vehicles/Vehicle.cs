﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;
using TotalAnigilationExamples.Interfaces;

namespace TotalAnigilationExamples.Vehicles
{
    public abstract class Vehicle : IGroundUnit
    {
        public string Name { get; set; }

        public double LifePercent { get; set; }

        public eFractions Fraction { get; set; }

        public string Tag { get; set; }

        public eClass TaClass { get; set; }

        public string BuiltBy { get; set; }

        public int TechLevel { get; set; }

        public int MetalCost { get; set; }

        public int EnergyCost { get; set; }

        public int BuildTime { get; set; }

        public double MaxVelocity { get; set; }

        public double Acceleration { get; set; }

        public int TurnRate { get; set; }

        public string Description { get; set; }

        public void Fire()
        {
            throw new NotImplementedException();
        }

        public string GetName()
        {
            return Name;
        }
        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void ShowDetails()
        {
            Console.WriteLine("Фракция: " + Fraction);
            Console.WriteLine("Тег: " + Tag);
            Console.WriteLine("Класс: " + TaClass);
            Console.WriteLine("Производится: " + BuiltBy);
            Console.WriteLine("Технический уровень: " + TechLevel);
            Console.WriteLine("Затраты металла: " + MetalCost);
            Console.WriteLine("Затраты энергии: " + EnergyCost);
            Console.WriteLine("Время постройки: " + BuildTime);
            Console.WriteLine("Максимальная скорость: " + MaxVelocity);
            Console.WriteLine("Ускорение: " + Acceleration);
            Console.WriteLine("Скорость поворота: " + TurnRate);
            Console.WriteLine("Описание: " + Description);
            Console.WriteLine("Процент жизни: " + LifePercent);
        }

        
    }
}
