﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;
using TotalAnigilationExamples.Vehicles;

namespace TotalAnigilationExamples.Vehicles
{
    class Spoiler : Vehicle
    {
        public Spoiler()
        {
            Fraction = eFractions.CORE;
            Name = "Spoiler";
            Tag = "CORMLV";
            TaClass = eClass.VEHICLE;
            BuiltBy = "VehiclePlant";
            TechLevel = 1;
            MetalCost = 187;
            EnergyCost = 1117;
            BuildTime = 1740;
            MaxVelocity = 9.6;
            Acceleration = 0.13;
            TurnRate = 38;
            Description = "The Spoiler is a level 1 unit that has the ability to build explosive landmines; the mines it builds vary in damage and range, with the shorter range/damage mines costing the least to build. It is relatively useless, due to its slow movement speed, so it can't build mines in different locations at a pace that would most likely be necessary. Its ARM counterpart is the Podger.";
            LifePercent = 100;
        }
    }
}
