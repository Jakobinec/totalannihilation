﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Vehicles
{
    class Slasher : Vehicle
    {
        public Slasher()
        {
            Fraction = eFractions.CORE;
            Name = "Slasher";
            Tag = "CORMIST";
            TaClass = eClass.VEHICLE;
            BuiltBy = "VehiclePlant";
            TechLevel = 1;
            MetalCost = 116;
            EnergyCost = 947;
            BuildTime = 1820;
            MaxVelocity = 17.4;
            Acceleration = 0.32;
            TurnRate = 84;
            Description = "he Slasher is a level 1 missile vehicle that functions primarily as an anti-air unit; it is not particularly well armoured and its weapon doesn't do much damage to ground units, but the range of the weapon allows it to fire from the back line and protect other units from incoming enemy aircraft. Its ARM counterpart is the Samson.";
            LifePercent = 100;
        }
    }
}
