﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;
using TotalAnigilationExamples.Vehicles;

namespace TotalAnigilationExamples.Vehicles
{
    class Weasel : Vehicle 
    {
        public Weasel()
        {
            Fraction = eFractions.CORE;
            Name = "Weasel";
            Tag = "CORFAV";
            TaClass = eClass.VEHICLE;
            BuiltBy = "VehiclePlant";
            TechLevel = 1;
            MetalCost = 38;
            EnergyCost = 575;
            BuildTime = 1515;
            MaxVelocity = 33.6;
            Acceleration = 0.43;
            TurnRate = 82;
            Description = "The Weasel is a level 1 vehicle created for the sole purpose of scouting; it is cheap to build and has high top speeds but provides very little in the way of combat due to its pitiful weapon power and near lack of any form of armour. Its ARM counterpart is the Jeffy.";
            LifePercent = 100;
        }
    }
}
