﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Interfaces;

namespace TotalAnigilationExamples
{
    class Squad
    {
        public List<IGroundUnit> Units { get; set; }

        public Squad() 
        {
            Units = new List<IGroundUnit>();        
        }
    }
}
