﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Kbots
{
    class Storm : Kbot
    {
        public Storm()
        {
            Fraction = eFractions.CORE;
            Tag = "CORSTORM";
            TaClass = eClass.KBOT;
            BuiltBy = "KbotLab";
            TechLevel = 1;
            MetalCost = 118;
            EnergyCost = 985;
            BuildTime = 1969;
            MaxVelocity = 15;
            Acceleration = 1.10;
            TurnRate = 165;
            Description = "The Storm is a level 1 rocket Kbot that provides basic ranged assault against enemy ground units; cheap, hardy and relatively quick to build, these units can prove quite useful on the field of combat. Its ARM counterpart is the Rocko. ";
            LifePercent = 100;
        }
    }
}
