﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Kbots
{
    class Crasher : Kbot
    {
        public Crasher()
        {
            Fraction = eFractions.CORE;
            Tag = "CORCRASH";
            TaClass = eClass.KBOT;
            BuiltBy = "KbotLab";
            TechLevel = 1;
            MetalCost = 129;
            EnergyCost = 1224;
            BuildTime = 2636;
            MaxVelocity = 15;
            Acceleration = 1.20;
            TurnRate = 167;
            Description = "The Crasher is a level 1 missile Kbot that provides basic defence against enemy Aircraft, and is relatively quick to build, but on the downside, does little damage to land-based units and has a low fire rate. The ARM counterpart to the Crasher is the Jethro. ";
            LifePercent = 100;
        }
    }
}
