﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Kbots
{
    class Thud : Kbot
    {
        public Thud()
        {
            Fraction = eFractions.CORE;
            Tag = "CORTHUD";
            TaClass = eClass.KBOT;
            BuiltBy = "KbotLab";
            TechLevel = 1;
            MetalCost = 147;
            EnergyCost = 1161;
            BuildTime = 2171;
            MaxVelocity = 13.6;
            Acceleration = 1.10;
            TurnRate = 165;
            Description = "The Thud is a level 1 artillery Kbot that has the necessary power to deal sizable damage to enemy ground units; it is somewhat cheap to build and should be considered when fighting wars early on in the game. It also has some of the best climbing abilities in the game, behind only the Commander and the ARM Spider. Its ARM counterpart is the Hammer. ";
            LifePercent = 100;
        }
    }
}
