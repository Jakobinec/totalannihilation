﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Kbots
{
    class AK : Kbot
    {
        public AK()
        {            
            Fraction = eFractions.CORE;
            Tag = "CORAK";
            TaClass = eClass.KBOT;
            BuiltBy = "KbotLab";
            TechLevel = 1;
            MetalCost = 56;
            EnergyCost = 696;
            BuildTime = 1523;
            MaxVelocity = 20.6;
            Acceleration = 1.10;
            TurnRate = 181;
            Description = "The A.K. is a basic CORE Kbot infantry unit; it is cheap to build and has high top speeds which is useful for scouting and taking out unguarded metal extractors, but its weak weapons and lack of armour make it ineffective at full scale combat.Its weapon is much weaker than its ARM counterpart, the Peewee.";
            LifePercent = 100;
        }

       
    }
}
