﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;


namespace TotalAnigilationExamples.Ship
{
    class Searcher : Ship 
    {
        public Searcher()
        {
            Fraction = eFractions.CORE;
            Tag = "CORPT";
            TaClass = eClass.SHIP;
            BuiltBy = "ShipLab";
            TechLevel = 1;
            MetalCost = 95;
            EnergyCost = 917;
            BuildTime = 1877;
            MaxVelocity = 43.2;
            Acceleration = 1.20;
            TurnRate = 86;
            Description = "The Searcher is a level 1 scout ship; it is very fast and maneuverable compared to other ships and, unlike other <scout> units, can actually be used for combat purposes, as its missile weapon is very effective against aircraft. It is quite cheap and fast to mass-produce as well, and in large numbers can protect larger ships from air harassment. Its ARM counterpart is the Skeeter.";
            LifePercent = 100;
        }
    }
}
