﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Ship
{
    class Envoy : Ship
    {
        public Envoy()
        {
            Fraction = eFractions.CORE;
            Tag = "CORTSHIP";
            TaClass = eClass.SHIP;
            BuiltBy = "ShipLab";
            TechLevel = 1;
            MetalCost = 887 ;
            EnergyCost = 4786;
            BuildTime = 13663;
            MaxVelocity = 21.6;
            Acceleration = 0.11;
            TurnRate = 21;
            Description = "The Envoy is a level 1 transport ship that can carry up to 20 units; it cannot pick up other ships as well as units that are submerged underwater. Because of its large capacity and the subsequent long loading/unloading time, it should never be put under enemy fire. Its ARM counterpart is the Hulk";
            LifePercent = 100;
        }
    }
}
