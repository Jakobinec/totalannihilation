﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Enums;

namespace TotalAnigilationExamples.Ship
{
    class Enforcer : Ship
    {
        public Enforcer()
        {
            Fraction = eFractions.CORE;
            Tag = "CORROY";
            TaClass = eClass.SHIP;
            BuiltBy = "ShipLab";
            TechLevel = 1;
            MetalCost = 887;
            EnergyCost = 4505;
            BuildTime = 13668;
            MaxVelocity = 35.2;
            Acceleration = 0.11;
            TurnRate = 21;
            Description = "The Enforcer is a level 1 destroyer ship that provides basic naval warfare; it is armed with a twin plasma battery that deals heavy damage but can be inaccurate at range, as well as depth charges and a built-in sonar. It is also quite fast, though it turns just as slowly as other large ships. Its ARM counterpart is the Crusader.";
            LifePercent = 100;
        }
    }
}
