﻿using System;
using System.Collections.Generic;
using System.Text;
using TotalAnigilationExamples.Kbots;

namespace TotalAnigilationExamples
{
    class KbotLab
    {
        public Kbot CreateKbot(eKbotTypes kbotType) 
        {
            switch (kbotType)
            {
                case eKbotTypes.AK:
                    return new AK();
                case eKbotTypes.Storm:
                    return new Storm();
                case eKbotTypes.Thud:
                   return new Thud();
                case eKbotTypes.Crasher:
                    return new Crasher();
            }

            return null;
        }
    }
}
